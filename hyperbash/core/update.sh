#!/bin/bash
# shellcheck source=/dev/null
#------------------
# Update functions
#------------------
function _which() {
    command -v "$1" &> /dev/null
}

# Check URL's
# ---------------------
function _url_exists() {
    if  _which wget; then
        if wget --spider "$1" 2>/dev/null; then
            return 0 # URL 'ok'
        else
            return 1 # URL 'fail'
        fi
    elif _which curl; then
        if curl --output /dev/null --silent --head --fail "$1"; then
            return 0 # URL 'ok'
        else
            return 1 # URL 'fail'
        fi
    fi
}

function _urls() {
    URL_1="https://libregit.org/heckyel/hyperbash"
    URL_2="https://notabug.org/heckyel/hyperbash"

    if [[ $(_url_exists "$URL_1") -eq 0 ]]; then
        URL="$URL_1"
        RAW="$URL_1/raw/branch/master"
    elif [[ $(_url_exists "$URL_2") -eq 0 ]]; then
        URL="$URL_2"
        RAW="$URL_2/raw/master"
    fi
}
# ----------------------

function ifexists_custom() {

    _urls "$@"

    if [ ! -e "$HOME/.hyperbash/_custom.sh" ]; then
        case $1 in
            wget) wget "$RAW/hyperbash/_custom.sh" -O "$HOME/.hyperbash/_custom.sh" ;;
            curl) curl "$RAW/hyperbash/_custom.sh" -o "$HOME/.hyperbash/_custom.sh" ;;
            git)  cp -v /tmp/hyperbash/hyperbash/_custom.sh "$HOME/.hyperbash/"     ;;
        esac
    fi
}

function updbashrc() {

    _urls "$@"

    # data integration
    if _which wget; then
        wget -nv "$RAW/hyperbash/hyperbash.sha512" -O "$HOME/.hyperbash/hyperbash.sha512" &> /dev/null
        ifexists_custom wget &> /dev/null
    elif _which curl; then
        curl "$RAW/hyperbash/hyperbash.sha512" -o "$HOME/.hyperbash/hyperbash.sha512" &> /dev/null
        ifexists_custom curl &> /dev/null
    fi

    # checksum of data verification
    (cd "$HOME/.hyperbash/" && sha512sum -c hyperbash.sha512 &> /dev/null)
    _interger=$?

    if _which git; then
        if [[ "$_interger" -eq 0 ]]; then
            # Import colors
            _colors_bash "$@"
            printf '%b' "${BOLD}${CYAN}"
            printf '%s\n' '    __  __                      __               __   '
            printf '%s\n' '   / / / /_  ______  ___  _____/ /_  ____ ______/ /_  '
            printf '%s\n' '  / /_/ / / / / __ \/ _ \/ ___/ __ \/ __ `/ ___/ __ \ '
            printf '%s\n' ' / __  / /_/ / /_/ /  __/ /  / /_/ / /_/ (__  ) / / / '
            printf '%s\n' '/_/ /_/\__, / .___/\___/_/  /_.___/\__,_/____/_/ /_/  '
            printf '%s\n' '      /____/_/                                        '
            printf '%s\n' '                                                      '
            printf '%b' "${BOLD}${GREY}"
            msg "¡Hurra! Hyperbash se ha actualizado y/o está en la versión actual." \
                "Hooray! Hyperbash has been updated and/or is at the current version."

            msg "Consigue tu copia de Hyperbash en: https://libregit.org/heckyel/hyperbash" \
                "Get your Hyperbash swag at: https://libregit.org/heckyel/hyperbash"
            printf '%b\n' "$RESET"
        else
            if [[ $(_url_exists "$URL") -eq 0 ]]; then
                # clone '--depth=1' not support cgit
                (git clone $URL /tmp/hyperbash/ --depth=1 &> /dev/null)
                printf '%s\r' "#####                   (33%)"
                sleep 1
                # core
                for i in autocomplete.sh colors.sh git.sh languages.sh status.sh update.sh; do
                    install -m644 /tmp/hyperbash/hyperbash/core/$i "$HOME/.hyperbash/core/$i"
                done
                # themes
                for i in default.sh light_theme.sh pure.sh special.sh; do
                    install -m644 /tmp/hyperbash/hyperbash/themes/$i "$HOME/.hyperbash/themes/$i"
                done
                # tools
                (cp -f /tmp/hyperbash/hyperbash/tools/* "$HOME/.hyperbash/tools/" &> /dev/null)

                for i in hyperbash.sh hyperbash.sha512; do
                    install -m644 /tmp/hyperbash/hyperbash/$i "$HOME/.hyperbash/$i"
                done
                (cp -f /tmp/hyperbash/.bash_profile "$HOME/" &> /dev/null)
                printf '%s\r' "#############           (66%)"
                (ifexists_custom git &> /dev/null)
                sleep 1
                (rm -fr /tmp/hyperbash/)
                printf '%s\n' "####################### (100%) done!"
                source "$HOME/.bashrc"
            else
                msg_err "El repo esta deshabilitado o no hay conexión a Internet" \
                        "The repo is disabled or connection failed"
                return 1
            fi
        fi
    else
        msg_err "No hay curl y git. Por favor, instale los programas para actualizar hyperbash" \
                "I couldn't find not curl and git. Please, install the programs to update hyperbash"
        return 1
    fi
}

function updbashrc_custom() {

    _urls "$@"

    if [[ $(_url_exists "$URL") -eq 0 ]]; then
        while true
        do
            function _copy_c() {
                if _which wget; then
                    wget "$RAW/hyperbash/_custom.sh" -O "$HOME/.hyperbash/_custom.sh"; source "$HOME/.bashrc"
                elif _which curl; then
                    curl "$RAW/hyperbash/_custom.sh" -o "$HOME/.hyperbash/_custom.sh"; source "$HOME/.bashrc"
                fi
            }

            question=$(msg "¿Estás seguro de sobre-escribir _custom.sh? [s/N]: " \
                           "Are you sure to overwrite _custom.sh? [y/N]: ")
            read -r -p "$question" input
            case $input in
                [sS]|[yY]) _copy_c "$@"; break ;;
                [nN]|"") break ;;
                *) msg "Por favor responde sí o no" \
                       "Please answer yes or no.";;
            esac
        done
    else
        msg_err "El repo esta deshabilitado o no hay conexión a Internet" \
                "The repo is disabled or connection failed"
        return 1
    fi
}

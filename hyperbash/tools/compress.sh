#!/bin/bash
# ex - archive extractor
# usage: ex <file>
function ex() {
    if [ -f "$1" ] ; then
        # shellcheck disable=SC2221,SC2222
        case "$1" in
            *.tar.bz2)   tar xjf "$1"    ;;
            *.tar.gz)    tar xzf "$1"    ;;
            *.tar.xz)    tar xf "$1"     ;;
            *.7z)        7z x "$1"       ;;
            *.bz2)       bunzip2 "$1"    ;;
            *.gz)        gunzip "$1"     ;;
            *.rar)       unar "$1"       ;;
            *.tar)       tar xf "$1"     ;;
            *.tbz2)      tar xjf "$1"    ;;
            *.tgz)       tar xzf "$1"    ;;
            *.Z)         uncompress "$1" ;;
            *.zip)       unzip "$1"      ;;
            *)           msg_err "No se puede extraer '$1' vía ex()" \
                                 "'$1' cannot be extracted via ex()"
                         return 1 ;;
        esac
    else
        msg_err "'$1' no es un archivo válido" \
                "'$1' is not a valid file"
        return 1
    fi
}

# Compress files or directories
function cex() {
    if [[ -f "$1"  || -d "$1" ]] ; then
        case ${LANG/_*/} in
            es)
                # Print viewport user
                printf '%s\n' "Elige una acción"
                printf '%s\n' "1) 7z"
                printf '%s\n' "2) bz2"
                printf '%s\n' "3) gz"
                printf '%s\n' "4) tar bz2"
                printf '%s\n' "5) tar gz"
                printf '%s\n' "6) tar xz"
                printf '%s\n' "7) tar"
                printf '%s\n' "8) tbz2"
                printf '%s\n' "9) tgz"
                printf '%s\n' "10) Z"
                printf '%s\n' "11) zip"
                printf '%s\n' "0) salir"
                printf "Inserta la opción aquí:"

                read -r A
                if [[ "$A" = 1 ]]; then
                    7z a "${1%/}.7z" "$1"
                elif [[ "$A" = 2 && -f "$1" ]]; then
                    bzip2 -k "$1"
                elif [[ "$A" = 3 && -f "$1" ]]; then
                    gzip --best --keep "$1"
                elif [[ "$A" = 4 ]]; then
                    (tar -c "$1" | bzip2 > "${1%/}.tar.bz2")
                elif [[ "$A" = 5 ]]; then
                    tar -czvf "${1%/}.tar.gz" "$1"
                elif [[ "$A" = 6 ]]; then
                    tar cJvf "${1%/}.tar.xz" "$1"
                elif [[ "$A" = 7 ]]; then
                    tar -cvf "${1%/}.tar" "$1"
                elif [[ "$A" = 8 ]]; then
                    (tar -c "$1" | bzip2 > "${1%/}.tbz2")
                elif [[ "$A" = 9 ]]; then
                    tar -czvf "${1%/}.tgz" "$1"
                elif [[ "$A" = 10 ]]; then
                    tar -czvf "${1%/}.z" "$1"
                elif [[ "$A" = 11 ]]; then
                    zip -r "${1%/}.zip" "$1"
                elif [[ "$A" = 0 ]]; then
                    printf '%s' "Que tenga un buen día :)"
                else
                    printf '%s' "Archivo inválido u Opción no listada"
                fi
                ;;

            *)
                # Print viewport user
                printf '%s\n' "Choose option"
                printf '%s\n' "1) 7z"
                printf '%s\n' "2) bz2"
                printf '%s\n' "3) gz"
                printf '%s\n' "4) tar bz2"
                printf '%s\n' "5) tar gz"
                printf '%s\n' "6) tar xz"
                printf '%s\n' "7) tar"
                printf '%s\n' "8) tbz2"
                printf '%s\n' "9) tgz"
                printf '%s\n' "10) Z"
                printf '%s\n' "11) zip"
                printf '%s\n' "0) exit"
                printf "Insert the option here:"

                read -r A
                if [[ "$A" = 1 ]]; then
                    7z a "${1%/}.7z" "$1"
                elif [[ "$A" = 2 && -f "$1" ]]; then
                    bzip2 -k "$1"
                elif [[ "$A" = 3 && -f "$1" ]]; then
                    gzip --best --keep "$1"
                elif [[ "$A" = 4 ]]; then
                    (tar -c "$1" | bzip2 > "${1%/}.tar.bz2")
                elif [[ "$A" = 5 ]]; then
                    tar -czvf "${1%/}.tar.gz" "$1"
                elif [[ "$A" = 6 ]]; then
                    tar cJvf "${1%/}.tar.xz" "$1"
                elif [[ "$A" = 7 ]]; then
                    tar -cvf "${1%/}.tar" "$1"
                elif [[ "$A" = 8 ]]; then
                    (tar -c "$1" | bzip2 > "${1%/}.tbz2")
                elif [[ "$A" = 9 ]]; then
                    tar -czvf "${1%/}.tgz" "$1"
                elif [[ "$A" = 10 ]]; then
                    tar -czvf "${1%/}.z" "$1"
                elif [[ "$A" = 11 ]]; then
                    zip -r "${1%/}.zip" "$1"
                elif [[ "$A" = 0 ]]; then
                    printf '%s' "You have a nice day :)"
                else
                    printf '%s' "Invalid file or Option not listed"
                fi
                ;;
        esac
    else
        msg_err "'$1' no es un archivo o directorio válido" \
                "'$1' is not a valid file or directory"
        return 1
    fi
}

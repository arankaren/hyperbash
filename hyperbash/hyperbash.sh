#!/bin/bash
#
# Custom Prompt Shell
#
# $HOME/.bashrc
#
# License: GNU GPLv3 or later
# See archive AUTHORS
#
# shellcheck disable=SC1090

xhost +local:root > /dev/null 2>&1

#---------------
# Check bash
#---------------
if [[ ! -f /bin/bash ]]; then
    if [[ $(command -v bash) != /bin/bash ]]; then
        printf '%s\n' "/bin/bash not found.  Please run 'sudo ln -s $(command -v bash) /bin/bash'"
    fi
fi

#----------------
# bash_aliases
#----------------
if [[ -f $HOME/.hyperbash/tools/aliases.sh ]]; then source "$HOME/.hyperbash/tools/aliases.sh"; else true; fi

#----------------
# bash_functions
#----------------
# [ core ]
if [[ -f $HOME/.hyperbash/core/autocomplete.sh ]]; then source "$HOME/.hyperbash/core/autocomplete.sh"; else true; fi
if [[ -f $HOME/.hyperbash/core/colors.sh ]]; then source "$HOME/.hyperbash/core/colors.sh"; else true; fi
if [[ -f $HOME/.hyperbash/core/git.sh ]]; then source "$HOME/.hyperbash/core/git.sh"; else true; fi
if [[ -f $HOME/.hyperbash/core/languages.sh ]]; then source "$HOME/.hyperbash/core/languages.sh"; else true; fi
if [[ -f $HOME/.hyperbash/core/status.sh ]]; then source "$HOME/.hyperbash/core/status.sh"; else true; fi
if [[ -f $HOME/.hyperbash/core/update.sh ]]; then source "$HOME/.hyperbash/core/update.sh"; else true; fi

#-------------
# Theme
#-------------
if [[ -f $HOME/.hyperbash/themes/default.sh ]]; then source "$HOME/.hyperbash/themes/default.sh"; else true; fi

#--------------
# bashrc_custom
#--------------
if [[ -f $HOME/.hyperbash/_custom.sh ]]; then source "$HOME/.hyperbash/_custom.sh"; else true; fi

#---------------
# Shell prompt
#---------------
if [[ -d $HOME/.hyperbash && -f $HOME/.hyperbash/_custom.sh && -s $HOME/.hyperbash/_custom.sh ]]; then
    PS1="${prompt:=$prompt}"
elif [[ -d $HOME/.hyperbash ]]; then
    PS1="${prompt:=$default}"
else
    PS1='[\u@\h \W]\$ '
fi
#Interactive Prompt
PS2="${_psi:=$_psi}"

# global unsets
unset SYMBOL prompt _psi

# clean up themes
unset default light_theme minterm pure special

# clean up colors
unset BLUE CYAN GREEN GREY LEMON ORANGE PURPLE \
      RED WHITE YELLOW BOLD RESET

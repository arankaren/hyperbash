#!/bin/bash

if [[ -n "$(ls -a ./hyperbash/)" && -f ./.bash_profile ]];
then
    cd ./hyperbash/ || exit

    var1=$(find ../ -name ".bash*" -type f -exec sha512sum {} \;)
    var2=$(find . -type f -name "*.sh" ! -iname "_custom.sh" -exec sha512sum {} \;)

    alist="echo $var1 $var2"
    $alist > tmp.txt

    output="fmt -w 160 tmp.txt"

    $output > hyperbash.sha512

    rm -rf tmp.txt

    sha512sum -c hyperbash.sha512

    printf '\e[1;36m%s\e[m\n' "success hyperbash.sha512"
else
    printf '\e[1;31m%s\e[m\n' "Error! files not found for verification"
fi
